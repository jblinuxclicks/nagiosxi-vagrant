# -*- mode: ruby -*-
# vi: set ft=ruby :

# Todo:
# - Fix empty hostname in VM description string
# - Resolve 'Public Key Not Installed' events
# - Make nagios configuration take into account the actual number of worker VMs deployed

puts "Execution started: " + Time.now.to_s

# get version
if File.file?('version')
  version_file = open('version', 'r')
  $env_version = (version_file.readline).strip
  version_file.close
else
  $env_version = "0.0.1-alpha"
end

# Set variable for current directory so VMs kan be grouped based on the name of the directory
current_dir = File.basename(Dir.getwd)

#Set base box for VMs
basebox = "bento/centos-7"

Vagrant.configure("2") do |config|
  config.vm.define "nagiosserver" do |nagiosserver|
    hostname = "nagiosserver"
    nagiosserver.vm.box = basebox
    config.vm.box_check_update=true
    nagiosserver.vm.hostname = hostname
    nagiosserver.vm.network :private_network, ip: "192.168.56.201"
    nagiosserver.vm.network "forwarded_port", guest: 22,  host: 2222, id: "ssh", disabled: true
    nagiosserver.vm.network "forwarded_port", guest: 22,  host: 2201, auto_correct: true
	  nagiosserver.vm.network "forwarded_port", guest: 80,  host: 8001, auto_correct: true
	  nagiosserver.vm.network "forwarded_port", guest: 443,  host: 4431, auto_correct: true
    $descriptionString="
    #{$hostname} v#{$env_version} based on CentOS 7
    base image #{$basebox}
    "
    nagiosserver.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--groups", "/#{current_dir}"]
      v.customize ["modifyvm", :id, "--memory", 512]
      v.customize ["modifyvm", :id, "--name", "vagrant-#{hostname}-v#{$env_version}"]
      v.customize ["modifyvm", :id, "--description", $descriptionString]
    end
    # Define ssh configuration
    nagiosserver.ssh.insert_key = false
    # Enable provisioning with a shell script. Additional provisioners such as
    # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
    # documentation for more information about their specific syntax and use.
    nagiosserver.vm.provision "vagrant", type: "shell" do |cmd|
      cmd.path = "./provision_scripts/vagrant_provision_server.sh"
    end
  end

  # Deploy worker VMs
  # Don't use more than 9 VMs as the IP configuration is limited to 192.168.56.11-192.168.56.19
  (1..2).each do |i|
    config.vm.define "nagiosworker-#{i}" do |nagiosworker|
      hostname = "nagiosworker-#{i}"
      $descriptionString="
      #{$hostname} v#{$env_version} based on CentOS 7
      base image #{$basebox}
      "
      nagiosworker.vm.box = basebox
      config.vm.box_check_update=true
      nagiosworker.vm.hostname = hostname
      nagiosworker.vm.network :private_network, ip: "192.168.56.1#{i}"
      nagiosworker.vm.provider :virtualbox do |v|
        v.customize ["modifyvm", :id, "--groups", "/#{current_dir}"]
        v.customize ["modifyvm", :id, "--memory", 512]
        v.customize ["modifyvm", :id, "--name", "vagrant-#{hostname}-v#{$env_version}"]
        v.customize ["modifyvm", :id, "--description", $descriptionString]
      end
      # Define ssh configuration
      nagiosworker.ssh.insert_key = false
      # Enable provisioning with a shell script. Additional provisioners such as
      # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
      # documentation for more information about their specific syntax and use.
      nagiosworker.vm.provision "vagrant", type: "shell" do |cmd|
        cmd.path = "./provision_scripts/vagrant_provision_worker.sh"
      end
    end
  end
end