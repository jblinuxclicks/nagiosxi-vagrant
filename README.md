# nagiosxi-vagrant
This is my very first Vagrant project. The goal is to create a collection of Vagrant projects so I have all the necessary build information to recreate my lab projects instead of keeping them on my hard-drive indefinitely.

The goal of the nagiosxi-vagrant project is to create a Nagios XI instance straight from the Nagios repositories https://repo.nagios.com/ This requires having the EPEL repository installed.

I'm using the [bento/centos-7](https://app.vagrantup.com/bento/boxes/centos-7) box as a base and based my Vagrantfile on the [vagrant-ubuntu-16.04-mate](https://github.com/cxtlabs/vagrant-ubuntu-16.04-mate) project from cxtlabs at GitHub.

After the VM is deployed you have a 60 day trial license to experiment with. If I'm not mistaken after the trial period expires the software will continue to function but you will have a 5 host limit.

As a bonus the project now has Mod Gearman installed on the Nagios XI server and provisions a number of worker VMs.

To test Mod Gearman first provision just the NagiosXI server like so.
```
vagrant up nagiosserver
```

Nagios is configured so that all checks on the localhost are executed by the internal Nagios scheduler instead of Mod Gearman. This way you can test the health of Mod Gearman even if it has issues. See the documentation at [Consol Labs](https://labs.consol.de/nagios/mod-gearman/index.html#_how_to_set_queue_by_custom_variable) for more information on how to achieve this by using Nagios custom object variables.

If you provision the rest of the VMs you can see the rest of the checks return an OK status.
```
vagrant up
```