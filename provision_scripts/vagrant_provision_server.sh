#!/bin/bash

# Disable IPv6 during provisioning because of issues with Nagios repo. Might be a good idea to disable permanently
sudo sysctl -w net.ipv6.conf.all.disable_ipv6=1
sudo sysctl -w net.ipv6.conf.default.disable_ipv6=1

# Install EPEL and Nagios repos
sudo yum -y install epel-release
sudo rpm -Uvh https://repo.nagios.com/nagios/7/nagios-repo-7-3.el7.noarch.rpm
sudo yum -y update

# Install Nagios XI and prerequisites for Nagios Core downgrade script
sudo yum -y install nagiosxi patch gcc glibc glibc-common gd gd-devel

# Downgrade Nagios Core in order to support Mod Gearman
# See following URL for more information: https://support.nagios.com/kb/article/nagios-xi-downgrading-nagios-core-823.html

# Stop Services
sudo systemctl stop nagios.service
sudo systemctl disable nagios.service
sudo rm -f /usr/lib/systemd/system/nagios.service
sudo systemctl daemon-reload

# Downgrade Nagios Core
sudo sed -i 's/^lock_file=.*/lock_file=\/usr\/local\/nagios\/var\/nagios.lock/g' /usr/local/nagios/etc/nagios.cfg
cd /tmp
sudo rm -rf nagiosxi xi*
sudo wget https://assets.nagios.com/downloads/nagiosxi/5/xi-5.4.13.tar.gz
sudo tar xzf xi-5.4.13.tar.gz
cd nagiosxi
sudo ./init.sh
cd subcomponents/nagioscore
sudo ./upgrade
sudo chmod +x /etc/init.d/nagios

# Update lock file locations
sudo sed -i 's/^lock_file=.*/lock_file=\/var\/run\/nagios.lock/g' /usr/local/nagios/etc/nagios.cfg
sudo sed -i 's/^NagiosRunFile=.*/NagiosRunFile=\/var\/run\/nagios.lock/g' /etc/init.d/nagios
sudo sed -i 's/^# pidfile.*/# pidfile: \/var\/run\/nagios.lock/g' /etc/init.d/nagios

# Start Services
sudo systemctl enable nagios.service
sudo systemctl restart nagios.service

# Install Mod Gearman
cd /tmp
sudo wget https://assets.nagios.com/downloads/nagiosxi/scripts/ModGearmanInstall.sh
sudo chmod +x ModGearmanInstall.sh
sudo ./ModGearmanInstall.sh --type=server

# Update Mod Gearman Server configuration
sudo sed -i 's/^encryption=.*/encryption=yes/g' /etc/mod_gearman2/module.conf
sudo sed -i 's/^key=.*/key=gearman_saves_the_day/g' /etc/mod_gearman2/module.conf
sudo sed -i 's/^#queue_custom_variable=.*/queue_custom_variable=WORKER/g' /etc/mod_gearman2/module.conf
# sudo sed -i 's/^localhostgroups=.*/localhostgroups=linux-servers/g' /etc/mod_gearman2/module.conf
sudo systemctl restart gearmand.service

# Update Mod Gearman Worker configuration but disable it by default
sudo sed -i 's/^encryption=.*/encryption=yes/g' /etc/mod_gearman2/worker.conf
sudo sed -i 's/^key=.*/key=gearman_saves_the_day/g' /etc/mod_gearman2/worker.conf
sudo systemctl stop mod-gearman2-worker.service
sudo systemctl disable mod-gearman2-worker.service

# Allow TCP/UDP through firewall
sudo iptables -I INPUT -p tcp --dport 4730 -j ACCEPT
sudo iptables -I INPUT -p udp --dport 4730 -j ACCEPT

# Modify Nagios configuration
sudo cp /vagrant/nagios_config/* /usr/local/nagios/etc/import/
sudo chmod 644 /usr/local/nagios/etc/import/*
sudo chown apache:nagios /usr/local/nagios/etc/import/*
sudo /usr/local/nagiosxi/scripts/reconfigure_nagios.sh