#!/bin/bash

# Disable IPv6 during provisioning because of issues with Nagios repo. Might be a good idea to disable permanently
sudo sysctl -w net.ipv6.conf.all.disable_ipv6=1
sudo sysctl -w net.ipv6.conf.default.disable_ipv6=1

# Install EPEL and Nagios repos
sudo yum -y install epel-release
sudo rpm -Uvh https://repo.nagios.com/nagios/7/nagios-repo-7-3.el7.noarch.rpm
sudo yum -y update

# Install nagios plugins
sudo yum -y install nagios-plugins-all

# Create symlink for NagiosXI compatibility
sudo mkdir /usr/local/nagios/
sudo ln -s /usr/lib64/nagios/plugins /usr/local/nagios/libexec

# Install Mod Gearman
cd /tmp
sudo wget https://assets.nagios.com/downloads/nagiosxi/scripts/ModGearmanInstall.sh
sudo chmod +x ModGearmanInstall.sh
echo "192.168.56.201" | sudo ./ModGearmanInstall.sh --type=worker

# Update Mod Gearman configuration
sudo sed -i 's/^encryption=.*/encryption=yes/g' /etc/mod_gearman2/worker.conf
sudo sed -i 's/^key=.*/key=gearman_saves_the_day/g' /etc/mod_gearman2/worker.conf
sudo systemctl restart mod-gearman2-worker.service

# Allow TCP/UDP through firewall
sudo iptables -I INPUT -p tcp --dport 4730 -j ACCEPT
sudo iptables -I INPUT -p udp --dport 4730 -j ACCEPT